use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::EOL 0.18

use Test::More 0.88;
use Test::EOL;

my @files = (
    'lib/Sub/Name.pm',
    't/00-report-prereqs.dd',
    't/00-report-prereqs.t',
    't/RT42725_deparse.t',
    't/RT96893_perlcc.t',
    't/smoke.t'
);

eol_unix_ok($_, { trailing_whitespace => 1 }) foreach @files;
done_testing;
